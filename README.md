# SELinux policy automation

This project tries to reduce the overhead of SELinux by automating the generation<br/>
of policy customization's on less SELinux optimized systems like Debian.

## Usage:
1. Let the system run/log in permissive mode and use it for ~1 week.

2. sudo -i<br/>
mkdir selinux && cd selinux<br/>
autopol.sh --init (create base dir's and policy's)<br/>
autopol.sh --update (update policy's)<br/>
autopol.sh --blob (create /var/log/message policy blob / rerun for update)<br/>
autopol.sh --clean (clean up /var/log/audit/audit.*)<br/>
autopol.sh --send_msg (create send_msg policy / rerun for update)
