#!/bin/bash

if [ $# -eq 0 ]; then
	exit 0
fi

if [ $1 == "--update" ]; then
	for dir in *; do
		dir="${dir%*/}"
		if [ -d "${dir}" ]; then
			echo -n "${dir}"": "
			bash "${dir}"/*.sh --update  2>/dev/null
		fi
	done
fi

if [ $1 == "--init" ]; then
    tmp_file="$(mktemp)"
    bool=false
    audit2allow -a | grep "#=" | awk '{print $2}' | sort | uniq > "${tmp_file}"
    while read line; do
        if [ -d "${line}" ] || [ "${line}" == "This" ]; then
            continue
        else
            echo "Do you want to create ""${line}""? (y/n/all)"
            read input < /dev/tty
            if [ "${input}" == "all" ]; then
                bool=true
            fi
            if [ "${input}" == "y" ] || [ "${bool}" = true ] ; then
                mkdir -p "${line}"
                sepolgen -n "${line}" -d "${line}" -p "${line}" --customize
                bash "${line}"/"${line}".sh
            fi
        fi
    done < "${tmp_file}"
    rm -f "${tmp_file}"
fi

if [ $1 == "--clean" ]; then
	> /var/log/audit/audit.*
fi

if [ $1 == "--blob" ]; then
	if [ ! -d "BLOB" ]; then
		(
		mkdir BLOB;
		cd BLOB;
		audit2allow -i /var/log/messages -M blob_t;
		semodule -i blob_t.pp
		)
	else
		(
		cd BLOB;
		audit2allow -i /var/log/messages -m blob_t | grep -v -e "module blob_t 1.0;" >> blob_t.te;
		make -f /usr/share/selinux/devel/Makefile;
		semodule -i blob_t.pp
		)
	fi
fi

if [ $1 == "--send_msg" ]; then
	if [ ! -d "send_msg" ]; then
		(
		mkdir send_msg;
		cd send_msg;
		ausearch --raw | grep -e "send_msg" | audit2allow -M send_msg;
		semodule -i send_msg.pp
		)
	else
		(
		cd send_msg;
		ausearch --raw | grep -e "send_msg" | audit2allow -m send_msg | grep -v -e "module send_msg 1.0;" >> send_msg.te;
		make -f /usr/share/selinux/devel/Makefile;
		semodule -i send_msg.pp
		)
	fi
fi

